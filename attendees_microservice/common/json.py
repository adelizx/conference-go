from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            new_dict = {}
            if hasattr(o, "get_api_url"):
                new_dict["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                new_dict[property] = value
            new_dict.update(self.get_extra_data(o))
            return new_dict
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
